# Personio Challenge
This project is solution for `Personio` coding challenge aimed at the position of `(Senior) Data Engineer/Architect`. 
The task was challenging; as it should be; which gives me motivation to complete it.

#### Table of Contents
    * [ETL Development](#development-setup)
    * [Run the project](#development-setup)
    * [Architecture](#architecture)
    * [Testing](#integration-test)
    * [Out of scope](#Out-of-scope)

## Development   
The below points are the project files for extracting the API data transforming it and loading it into sqlite 
    * The project entry point main.py file
    * Makefile
    * Architecture

## To run the project follow the below points
    * make requirements 
        it will create the venv and install python libraries
    * make run 
        run the main.py 
    * make clean 
        clean the cache and delete the venv 

#### Python
Python script is setting the connection to chargebee API then it gets the data in json format,
Then it parses the data filtering on the requested columns only, Data is being accumulated in a list to make a bulk insert in one single database connection,

## Architecture
I used Python and sqlite
a diagram is attached to explain my design.
[Schematic Architecture](diagrams/architecture.png)

## Testing
I have created an integration test to ensure that the script is connecting to the sqlite and inserting into the database.
    * test_table_existence
    * test_database_connection

## Out of scope
- Creating initial load and incremental load functions ( it will be as future work )
