"""
It handles the API connection and send the API request
"""
from configparser import ConfigParser
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

def initialize_api_connection():
    """
    Initialize the API connection which reads the credentials from the ini file.
    """
    parser = ConfigParser()
    parser.read("cred.ini")
    url_parser = parser.get("chargebee_api", "url")
    token_parser = parser.get("chargebee_api", "token")
    return url_parser, token_parser

def request_api(url_param, token_param):
    """
    Calling the API using HTTP authentication
    using the token and empty password,
    retry the session Exponentially 5s,10s,20s
    based on AWS exponential backoff in AWS.
    link : https://docs.aws.amazon.com/general/latest/gr/api-retries.html
    """
    try:
        session_request = requests.Session()
        retries = Retry(total=5, backoff_factor=10, status_forcelist=[400, 503])
        session_request.mount(url_param, HTTPAdapter(max_retries=retries))
        api_data = session_request.get(
            url_param,
            auth=HTTPBasicAuth(token_param, ""),
        )
    except session_request.exceptions.RequestException as error:
        raise SystemExit("An errors happened in " + str(error)) from error
    return api_data