"""
Transforming and filtering the data, extracting and parsing the requested
columns from the json API.
"""
from datetime import datetime

def parse_json_api(json_data):
    parsed_data = []
    invoce_list = json_data['list']
    try:
        for i in invoce_list:
            invoice_obj = i['invoice']
            customer_id = invoice_obj['customer_id']
            invoice_date = invoice_obj['date']
            invoice_date = datetime.fromtimestamp(invoice_date)
            invoice_due_date = invoice_obj['due_date']
            invoice_due_date = datetime.fromtimestamp(invoice_due_date)
            invoice_id = invoice_obj['id']
            invoice_status = invoice_obj['status']
            invoice_subscription_id = invoice_obj['subscription_id']
            obj_parsed_tuple = (customer_id ,invoice_date ,invoice_due_date ,
            invoice_id ,invoice_status ,invoice_subscription_id )
            # nested loop over the line_items per invoice
            # As line_items contains multiple items for same invoice So
            # i append the line_items on each invoice in new object
            # as line items contains multiple items.
            for inner_loop in invoice_obj['line_items']:
                invoice_line_id = inner_loop['id']
                invoice_line_items_unit_amount = inner_loop['unit_amount']
                invoice_line_items_quantity = inner_loop['quantity']
                invoice_line_items_pricing_model = inner_loop['pricing_model']
                invoice_line_items_entity_id = inner_loop['entity_id']
                invoice_line_items_discount_amount = inner_loop['discount_amount']
                parsed_line_items =(invoice_line_id, invoice_line_items_unit_amount ,
                                    invoice_line_items_quantity ,invoice_line_items_pricing_model ,
                                    invoice_line_items_entity_id ,invoice_line_items_discount_amount)
                parsed_data.append(obj_parsed_tuple+parsed_line_items)
    except Exception as error:
        raise SystemExit("An errors happened in "+str(error) ) from error
    return parsed_data
