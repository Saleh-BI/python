'''
This file is running
- config_call_api: to setup the api connection and send the api call.
- JsonParser: parsing the json file and filter on the requsted columns
                        return parsed json data as object.
- sqlite_database: it connects to the sqlite ,
    create and insert the records in the database.
'''
from config_call_api import initialize_api_connection ,request_api
from json_parser import parse_json_api
from sqlite_database import create_connection ,create_table ,insert_data

url, token = initialize_api_connection()
api_requested_data = request_api(url, token)

json_data=api_requested_data.json()
parsed_data = parse_json_api(json_data)

sqlite_connection,sqlite_cursor = create_connection()
create_table(sqlite_connection ,sqlite_cursor)
insert_data(sqlite_cursor ,sqlite_connection ,parsed_data)
