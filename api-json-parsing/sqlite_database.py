'''
- In this script, the database connection is being established.
- Table invoices_chargebee will be created if not created before.
- It inserts the data into sqlite as bulk load.
'''
import sqlite3

def create_connection():
    """ create a database connection to the SQLite database
    :return: Connection object or None
    """
    connection = None
    try:
        connection = sqlite3.connect("test.db")
        cursor = connection.cursor()
    except sqlite3.Error as error:
        print("Failed to insert multiple records into sqlite table", error)
    return connection,cursor

def create_table(database_connection ,database_cursor):
    """
    Create a new project into the projects table
    :param database_connection:
    :param database_cursor :
    """
    #create table invoices_chargebee
    query='''CREATE TABLE IF NOT EXISTS invoices_chargebee
            (customer_id CHAR(25),
            invoice_date DATETIME,
            invoice_due_date DATETIME,
            invoice_id INT,
            invoice_status CHAR(15),
            invoice_subscription_id CHAR(25),
            invoice_line_id CHAR(25), 
            invoice_line_items_unit_amount INT,
            invoice_line_items_quantity INT,
            invoice_line_items_pricing_model CHAR(25),
            invoice_line_items_entity_id CHAR(50),
            invoice_line_items_discount_amount INT)'''
    database_cursor.execute(query)
    database_connection.commit()

def insert_data(db_cursor ,db_connection ,parsed_recordlist):
    """
    insert the parsed data into sqlite
    :param db_cursor:
    :param db_connection:
    :param parsed_recordlist:
    """
    #create table invoices_chargebee
    sqlite_insert_query = """INSERT INTO invoices_chargebee
                            (customer_id,invoice_date,invoice_due_date,invoice_id,
                            invoice_status,invoice_subscription_id,invoice_line_id, invoice_line_items_unit_amount ,
                                    invoice_line_items_quantity ,invoice_line_items_pricing_model ,
                                    invoice_line_items_entity_id ,invoice_line_items_discount_amount) 
                            VALUES (?, ?, ?, ? ,?, ?, ?, ? , ?, ?, ?, ?);"""
    db_cursor.executemany(sqlite_insert_query, parsed_recordlist)
    db_connection.commit()
    print("Total",db_cursor.rowcount,"Records inserted successfully \
        into invoices_chargebee table",)
    db_connection.commit()
    db_cursor.close()
