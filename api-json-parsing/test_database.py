'''
Integration testing 
To test the insertion function and the connection, Table invoices_chargebee_test has been created 
I truncate the table getting that count(*) is zero and then i insert and count 
'''
import sqlite3
import unittest
import json

class TestAPIParsing(unittest.TestCase):
    connection = sqlite3.connect("test.db")
    database_cursor = connection.cursor() 

    def test_database_connection(self):
        '''
        test the sqlite connectivity by testing the current sqlite version.
        '''
        self.database_cursor.execute('select sqlite_version();')
        actual =self.database_cursor.fetchall()
        expected = '3.32.3'
        self.assertIn(expected ,str(actual))

    def test_table_existence(self):
        '''
        test the sqlite connectivity by testing the current sqlite version.
        '''
        self.database_cursor.execute('SELECT name FROM sqlite_master WHERE type="table" ')
        actual = self.database_cursor.fetchall()
        expected = 'invoices_chargebee'
        for i in actual:
            self.assertIn(expected, str(i))
    
if __name__ == '__main__':
    unittest.main()
    