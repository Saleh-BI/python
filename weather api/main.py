from urllib.request import urlopen
import pandas as pd
import json 
import psycopg2
import logging
import os
import sys
from datetime import date, timedelta
import datetime
import time
import os

# configurations 
# API key and endpoint 
# The configurations could be sent in config file for future work
# Loading args
loading_type=sys.argv[1]

access_key_id = 'bebfc428dca4e2b3168bd215d96cea65'
host="localhost"
dbname="postgres"
user="postgres"
port=5466
password="postgres"
# get current directory 
current_directory = os.path.dirname(os.path.abspath(__file__))
# local file which will be loaded into postgres
local_file_name =current_directory+'/weather_berlin.csv'

# setting connection with postgres
def postgres_connection():
        conn = psycopg2.connect(host=host,
                                dbname=dbname,
                                user=user,
                                port=port,
                                password=password)
        cur = conn.cursor()
        return cur,conn

# setting connection with postgres       
def get_data(api_date):
    retry=0
    try: 
        URL_link = 'https://api.openweathermap.org/data/2.5/weather?q=berlin&units=metric&appid='+ access_key_id
        response = urlopen(URL_link)
        elevations = response.read()
        data = json.loads(elevations)
        return data 
    except Exception:
        # Handling if failuer happen in the api call 
        # i take the last date and i pass it to the function again after 30 seconds
        time.sleep(30)
        if retry <= 3: 
            retry+=1
            get_data(api_date) 
        else :
            exit(1)


# save the dataframe into csv locally
def save_data_locally(data,writing_mode):
    df = pd.json_normalize(data)
    # Getting only the tempreture and city name 
    df_city_temp = df[["main.temp","name"]]
    df_city_temp['date']=pd.to_datetime(df["dt"] ,unit='s')
    df_city_temp.to_csv (local_file_name, index = False, mode=writing_mode , header=False)
   
def check_local_file_size():
    if os.path.getsize(local_file_name) == 0:
        file_status=0
    else:
        file_status=1
    return file_status


# loading the file into postgres 
def load_into_postgres(cur,conn):
    with open(local_file_name, 'r') as f:
        cur.copy_from(f,"tempreture", sep=',', null='')
    f.close()

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
              yield start_date + timedelta(n)

def execute_query(cur,con,query):              
    cur.execute(query)

def main():
    # To get today's date from the date range function 
    today_date = date.today()
    tomorrow_date = date.today()+ datetime.timedelta(days=1)

    # setup postgres connection
    cur,conn=postgres_connection()

    # delete the files if exist not to have duplicate data 
    try:
        os.remove(local_file_name)
    except OSError:
        pass

    if loading_type == 'incremental' :
        # Today's rate 
        api_date = str(today_date)
        # get api data
        data=get_data(api_date)
        if len(data) != 0: 
            # convert the json data into dataframe and save it into a local file
            save_data_locally(data,'w')
            #Deleting current date data if exist
            query="""delete from public.tempreture where date = current_date ; """ 
            execute_query(cur,conn,query)

    # elif loading_type == 'initial' :
    #     # initial load 
    #     start_date=sys.argv[2]
    #     start_date=datetime.datetime.strptime(start_date, "%Y-%m-%d")
    #     start_date = start_date.date()
    #     # loop over the date range and append the range in the list
    #     for dt in daterange(start_date, today_date):
    #         # get api data
    #         data=get_data(str(dt))
    #         # convvert the json data into dataframe and save it into a local file
    #         save_data_locally(data,'a')
    #         # Truncate the table only if we have data in the local file 
    #         # so first i check the file size if > 0 
    #         file_size_status=check_local_file_size()
    #         if file_size_status != 0: 
    #             query="""truncate table public.tempreture; """ 
    #             execute_query(cur,conn,query)
    # else :
    #     print('please enter incremental or initial ')

    # load the local file into postgres 
    # if the program succeeded 
    load_into_postgres(cur,conn)
    
    conn.commit()
    conn.close()
    
if __name__ == '__main__':
    try:
        main()
    except Exception as err:
        print(str(err))
        logging.error("An error happened " + str(err))
        exit(1)